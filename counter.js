const counter = document.querySelector(".counter__value");
const counterDec = document.querySelector(".counter__button--dec");
const counterInc = document.querySelector(".counter__button--inc");
const counterReset = document.querySelector(".counter__button--reset");

const increment = () => {
  counter.innerHTML = (parseInt(counter.innerHTML) + 1).toString();
};

const decrement = () => {
  counter.innerHTML = (parseInt(counter.innerHTML) - 1).toString();
};

const reset = () => {
  counter.innerHTML = "0";
};

counterInc.addEventListener("click", increment);
counterDec.addEventListener("click", decrement);
counterReset.addEventListener("click", reset);
